/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sencageek.pojotablecontrol;

import com.panemu.tiwulfx.common.TableCriteria;
import com.panemu.tiwulfx.common.TableData;
import com.panemu.tiwulfx.table.TableControl;
import com.panemu.tiwulfx.table.TableController;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TableColumn;
import javafx.scene.layout.StackPane;

/**
 *
 * @author Sencageek <sencageek@gmail.com>
 */
public class FrmEmployee extends StackPane {

	@FXML
	private TableControl<Employee> tblEmployee;

	FrmEmployee() {
		//load fxml file
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Scene.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);
		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
		init();
	}
	
	private void init() {
		tblEmployee.setController(new CntlRecord());
		tblEmployee.setRecordClass(Employee.class);
	}

	public void reload() {
		tblEmployee.reloadFirstPage();
	}

	private class CntlRecord extends TableController<Employee> {

		@Override
		public TableData<Employee> loadData(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<TableColumn.SortType> sortingOrders, int maxResult) {
			//create list of Employee that will be displayed
			List<Employee> listEmployee = new ArrayList<>();
			// instantiate Employee
			Employee employee = new Employee();
			employee.setName("Janaka");
			employee.setAddress("Jakarta");
			employee.setSalary(1000);
			listEmployee.add(employee);
			
			employee = new Employee();
			employee.setName("Parikesit");
			employee.setAddress("Yogyakarta");
			employee.setSalary(2000);
			listEmployee.add(employee);
			
			employee = new Employee();
			employee.setName("Sencaki");
			employee.setAddress("Garboruci");
			employee.setSalary(3000);
			listEmployee.add(employee);
			
			return new TableData<>(listEmployee, false, listEmployee.size());
		}

	}

}
